// @ts-nocheck
import React, { useState, useEffect } from 'react';

// const host = `http://localhost:3000`
// const host = `http://167.172.96.190:3000`
const host = `http://157.230.24.206:3000`

// Authentication component
const Login = ({ onLogin }) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleLogin = () => {
    // Implement login logic
    // Example: Call login API endpoint
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/x-www-form-urlencoded");

    const urlencoded = new URLSearchParams();
    urlencoded.append("username", username);
    urlencoded.append("password", password);

    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: urlencoded,
      redirect: "follow"
    };

    fetch(`${host}/login`, requestOptions)
      .then((response) => response.text())
      .then((result) => console.log(result))
      .catch((error) => console.error(error));
  };

  return (
    <div>
      <input type="text" placeholder="Username" value={username} onChange={e => setUsername(e.target.value)} />
      <input type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)} />
      <button onClick={handleLogin}>Login</button>
    </div>
  );
};

// File upload component
const Upload = ({ accessToken, setUploadDone }) => {
  const [file, setFile] = useState(null);
  const [tags, setTags] = useState('');

  const handleFileChange = e => {
    setFile(e.target.files[0]);
  };

  const handleUpload = () => {
    // Implement file upload logic
    // Example: Use FormData to upload file
    const formData = new FormData();
    formData.append('file', file);
    formData.append('tags', tags);

    fetch(`${host}/upload`, {
      method: 'POST',
      headers: {
        'Authorization': `Bearer ${accessToken}`
      },
      body: formData
    })
      .then(response => {
        if (response.ok) {
          console.log('File uploaded successfully');
          // Reset form after successful upload
          setFile(null);
          setTags('');
          // setUploadDone(dummyCount++)
          window.location.reload()
        } else {
          console.error('Error uploading file:', response.statusText);
        }
      })
      .catch(error => console.error('Error uploading file:', error));
  };

  return (
    <div>
      <p>You may drag and drop a file on the Choose File button</p>
      <input type="file" accept="image/*, video/*" onChange={handleFileChange} />
      <input type="text" placeholder="Tags (comma-separated)" value={tags} onChange={e => setTags(e.target.value)} />
      <button onClick={handleUpload}>Upload</button>
    </div>
  );
};

const FileManager = ({ accessToken, onUploadDone }) => {
  const [files, setFiles] = useState([]);

  // Fetch list of files for the user using the access token
  useEffect(() => {
    const fetchFiles = async () => {
      try {
        const response = await fetch(`${host}/files`, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${accessToken}`
          }
        });
        if (!response.ok) {
          throw new Error('Failed to fetch files');
        }
        const data = await response.json();
        setFiles(data);
      } catch (error) {
        console.error('Error fetching files:', error);
      }
    };

    setInterval(() => {
      fetchFiles();
    }, 10 * 1000)
  }, [accessToken]);

  useEffect(() => {
    console.log(`onUploadDone`)
    const fetchFiles = async () => {
      try {
        const response = await fetch(`${host}/files`, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${accessToken}`
          }
        });
        if (!response.ok) {
          throw new Error('Failed to fetch files');
        }
        const data = await response.json();
        setFiles(data);
      } catch (error) {
        console.error('Error fetching files:', error);
      }
    };

    fetchFiles();
  }, [onUploadDone])

  // Function to generate shareable link for a file
  const generateShareLink = fileId => {
    // // Implement shareable link generation logic
    // // Example: Call API endpoint to generate link
    // fetch(`${host}/files/${fileId}/share`, {
    //   method: 'GET',
    //   headers: {
    //     'Authorization': `Bearer ${accessToken}`
    //   }
    // })
    //   .then(response => response.json())
    //   .then(data => {
    //     console.log('Shareable link:', data.link);
    //     alert('Shareable link:', data.link)
    //   })
    //   .catch(error => console.error('Error generating shareable link:', error));

    console.log(`${host}/view/${fileId}`)
    alert(`You can access the image via: ${host}/view/${fileId}`)
  };

  // Function to track file view statistics
  const trackFileView = fileId => {
    // Implement file view tracking logic
    // Example: Call API endpoint to track file view
    fetch(`${host}/files/${fileId}/view`, {
      method: 'POST',
      headers: {
        'Authorization': `Bearer ${accessToken}`
      }
    })
      .then(response => {
        if (response.ok) {
          console.log('File view tracked successfully');
        } else {
          console.error('Error tracking file view:', response.statusText);
        }
      })
      .catch(error => console.error('Error tracking file view:', error));
  };

  // Function to handle file drop
  const handleDrop = e => {
    e.preventDefault();
    const droppedFiles = Array.from(e.dataTransfer.files);
    console.log('Dropped files:', droppedFiles);
    // Implement file upload logic for dropped files
  };

  // Function to handle drag over
  const handleDragOver = e => {
    e.preventDefault();
  };

  return (
    <div>
      <h2>File Manager</h2>
      <ul>
        {files.map(file => (
          <li key={file._id}>
            <strong>Filename:</strong> {file.filename}, <strong>Mimetype:</strong> {file.mimetype}, <strong>Views:</strong> {file.views} times
            <button onClick={() => generateShareLink(file._id)}>Generate Shareable Link</button>
          </li>
        ))}
      </ul>

    </div>
  );
};


// Main App component
const App = () => {
  const [accessToken, setAccessToken] = useState(null);
  const [onUploadDone, setUploadDone] = useState(0)

  const handleLogin = token => {
    setAccessToken(token);
    localStorage.setItem("token", token)
  };

  useEffect(() => {
    setAccessToken(localStorage.getItem("token"));
  }, [])

  return (
    <div>
      {!accessToken ? <Login onLogin={handleLogin} /> : (
        <>
          <Upload accessToken={accessToken} setUploadDone={setUploadDone} />
          <FileManager accessToken={accessToken} onUploadDone={onUploadDone} />
        </>
      )}
    </div>
  );
};

export default App;
