import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react-swc'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  server: {
    host: true,
    port: 80, // Development server runs on port 80
    strictPort: true,
    hmr: {
      host: "157.230.24.206",
      port: 80,
    },
  },
  preview: {
    host: true,
    port: 80, // Preview server also runs on port 80
    strictPort: true,
  },
})
